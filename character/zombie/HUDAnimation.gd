extends AnimationPlayer

func _ready():
	owner.connect("heart_collected", self, "queue", ["heart_collected"])
	owner.connect("won", self, "queue", ["win"])
	owner.connect("lost", self, "queue", ["lost"])
