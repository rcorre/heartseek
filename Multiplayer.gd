extends Node

const DEFAULT_PORT := 43278  # HEART
const MAX_PLAYERS := 8
const GAME_SCENE := preload("res://level/base/Base.tscn")
const MENU_SCENE := preload("res://menus/MainMenu.tscn")

const _ERROR_MSG = {
	ERR_ALREADY_IN_USE : "Already in use",
	ERR_CANT_CREATE    : "Can't create",
}

# maps each player's network unique ID to a node name
var players := {}

# Store node name for consistency across reconnections
var my_name := ""

var started := false
var game_over := false

func _fail(err: int):
	var msg: String = _ERROR_MSG.get(err, "Unknown error %d" % err)
	printerr("host_error: ", msg)

func _ready():
	get_tree().connect("connected_to_server", self, "_on_connected_to_server")
	get_tree().connect("connection_failed", self, "_on_connection_failed")
	get_tree().connect("network_peer_connected", self, "_on_network_peer_connected")
	get_tree().connect("network_peer_disconnected", self, "_on_network_peer_disconnected")
	get_tree().connect("server_disconnected", self, "_on_server_disconnected")

func _on_connected_to_server():
	# If I'm reconnecting, I want to request the same node
	rpc_id(1, "request_register", my_name)

func _on_connection_failed():
	pass

func _on_network_peer_connected(netid: int):
	# When the host notices a peer connect,
	# look for a bot to replace with the player
	# then notify everyone
	if not get_tree().is_network_server():
		print_debug("Not host, ignoring network_peer_connected")
		return
	print_debug("registering player with netid=", netid)
	var used := players.values()
	for node in all_players():
		if not used.has(node.name):
			print_debug("assigning netid=", netid, " to playerid=", node.name)
			players[netid] = node.name
			rpc("update_players", players)
			return

func _on_network_peer_disconnected(netid: int):
	if not get_tree().is_network_server():
		print_debug("Not host, ignoring network_peer_connected")
		return
	print_debug("host: removing netid: ", netid)
	players.erase(netid)
	rpc("update_players", players)

func _on_server_disconnected():
	if game_over:
		print_debug("Ignoring server disconnect, game is over")
		return
	# arbitrarily pick the lowest netid as the new host
	print_debug("Server disconnected. Pausing game and looking for new host")
	get_tree().paused = true
	players.erase(1)  # erase the host
	var ids := players.keys()
	ids.sort()
	players = {}
	print_debug("Selecting host with netid=", ids[0])
	if local_netid() == ids[0]:
		print_debug("I'm the new host")
		host()
		_maybe_begin_game()
	else:
		print_debug("I'm not the new host")
		join_new_lobby()

func join_new_lobby(tries := 10):
	print_debug("Looking for new lobby, tries=", tries)
	var fetch := GotmLobbyFetch.new()
	var lobbies = yield(fetch.first(), "completed")
	if len(lobbies) > 0:
		join(lobbies[0])
	elif tries > 0:
		get_tree().create_timer(1.0).connect("timeout", self, "join_new_lobby", [tries - 1])
	else:
		push_warning("Failed to find new lobby, hosting new game")
		host()

puppet func update_players(new_players: Dictionary):
	print_debug("updating player map: ", new_players)
	players = new_players
	my_name = players[local_netid()]

func start():
	# Multiplayer only enabled on gotm.io
	if not Gotm.is_live():
		print_debug("Gotm.io not live, starting local game")
		_maybe_begin_game()
		return

	# look for a lobby and join it if possible
	# otherwise try to start a new lobby
	print_debug("Fetching Gotm.io lobbies")
	var fetch := GotmLobbyFetch.new()
	var lobbies = yield(fetch.first(), "completed")
	if len(lobbies) == 0:
		print_debug("No lobbies, hosting game")
		host()
	else:
		print_debug("Joining lobby: ", lobbies[0].id)
		join(lobbies[0])

func host_local():
	var peer := NetworkedMultiplayerENet.new()
	peer.set_compression_mode(NetworkedMultiplayerENet.COMPRESS_RANGE_CODER)
	var err := peer.create_server(DEFAULT_PORT, MAX_PLAYERS)
	if err != OK:
		_fail(err)
		return

	get_tree().set_network_peer(peer)
	_maybe_begin_game()

	my_name = "0"
	players[peer.get_unique_id()] = "0"

func join_local():
	var peer := NetworkedMultiplayerENet.new()
	peer.set_compression_mode(NetworkedMultiplayerENet.COMPRESS_RANGE_CODER)
	var err := peer.create_client("127.0.0.1", DEFAULT_PORT)
	if err:
		_fail(err)
		return
	get_tree().set_network_peer(peer)

func host():
	Gotm.host_lobby(false)
	Gotm.lobby.hidden = false
	var peer := NetworkedMultiplayerENet.new()
	peer.set_compression_mode(NetworkedMultiplayerENet.COMPRESS_RANGE_CODER)
	var err := peer.create_server(DEFAULT_PORT, MAX_PLAYERS)
	if err != OK:
		_fail(err)
		return

	if my_name == "":
		my_name = "0"
	players[peer.get_unique_id()] = my_name
	get_tree().set_network_peer(peer)
	_maybe_begin_game()
	print_debug("host complete")

func join(lobby: GotmLobby):
	var err = yield(lobby.join(), "completed")
	if err != OK:
		_fail(err)
		return
	var peer := NetworkedMultiplayerENet.new()
	peer.set_compression_mode(NetworkedMultiplayerENet.COMPRESS_RANGE_CODER)
	err = peer.create_client(lobby.host.address, DEFAULT_PORT)
	if err:
		_fail(err)
		return
	get_tree().set_network_peer(peer)
	print_debug("join complete")

func _maybe_begin_game() -> bool:
	get_tree().paused = false
	if not started:
		started = true
		get_tree().change_scene_to(GAME_SCENE)
		return true
	return false

func all_players() -> Array:
	return get_tree().get_nodes_in_group("player")

func local_netid() -> int:
	return get_tree().get_network_unique_id()

func player_with_id(name: String) -> Node:
	for p in all_players():
		if p.name == name:
			return p
	push_error("Player not found for name=%s" % name)
	return null

func local_player() -> Node:
	return player_with_id(my_name)

func is_online() -> bool:
	var peer := get_tree().network_peer
	return peer != null and peer.get_connection_status() == peer.CONNECTION_CONNECTED

func _physics_process(_delta: float):
	if not is_online() or not started:
		return

	if get_tree().is_network_server():
		var state := {}
		for p in all_players():
			state[p.name] = p.get_state()
		rpc_unreliable("update_guest_state", state)
		return

	var p := local_player()
	if p:
		rpc_id(1, "update_server_state", p.get_state())

puppet func update_guest_state(state: Dictionary):
	if _maybe_begin_game():
		# just started, need to set state after it loads
		call_deferred("update_guest_state", state)
	else:
		for p in all_players():
			p.set_state(state[p.name])

master func update_server_state(state: Dictionary):
	var netid := get_tree().get_rpc_sender_id()
	var playerid := players[netid] as String
	if playerid == null:
		push_error("Local player not found for netid=%d in %s" % [netid, players])
	player_with_id(playerid).set_state(state)

# A peer sends request_register to the host after connecting
# If it is reconnecting, it can request the same node_name it had before
# On a first connection, node_name is empty and the server will assign it
master func request_register(node_name: String):
	var netid := get_tree().get_rpc_sender_id()
	var used := players.values()
	print_debug("netid=", netid, "requested name: ", node_name)
	if node_name and not used.has(node_name):
		players[netid] = node_name
	else:
		for node in all_players():
			if not used.has(node.name):
				print_debug("assigning netid=", netid, " to playerid=", node.name)
				players[netid] = node.name
				break

	# let everyone know about the new player
	rpc("update_players", players)

func end_game():
	game_over = true
	yield(get_tree().create_timer(9.0), "timeout")
	if get_tree().is_network_server():
		Gotm.lobby.leave()
	get_tree().network_peer = null
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	players = {}
	my_name = ""
	started = false
	game_over = false
	get_tree().change_scene_to(MENU_SCENE)
