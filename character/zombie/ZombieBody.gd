extends Spatial

func _ready():
	owner.connect("revived", self, "hide")
	owner.connect("zombified", self, "show")

	# first player is the zombie
	set_visible(owner.is_zombie)
