extends KinematicBody

const MOUSE_SENSITIVITY := 0.003
const ACCEL := 10.0
const GRAVITY := 9.8
const LOOK_SPEED := 12.0
const MAX_HEARTS := 3
const ZOMBIFY_TIME := 3.0

signal heart_collected()
signal won()
signal lost()
signal died()
signal revived()
signal zombified()

onready var camera: Spatial = $Armature/Skeleton/BoneAttachment/Camera/CameraPivot
onready var anim_tree: AnimationTree = $AnimationTree

var motion := Vector2()
var look_dir := Vector2()
var got_initial_server_state := false
var hearts := 0
var is_zombie := false

func is_first_player():
	return name == "0"

func _enter_tree():
	add_to_group("player")
	is_zombie = is_first_player()

func is_local() -> bool:
	if get_tree().network_peer:
		return Multiplayer.my_name == name
	# for offline play, just control playerid=0
	return is_first_player()

func _unhandled_input(ev: InputEvent):
	if not is_local() or Multiplayer.game_over:
		return

	var button := ev as InputEventMouseButton
	if button:
		# allow recapture if the player has clicked away
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		return

	if Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED:
		# ignore if the mouse moves over the screen but isn't captured
		return

	var mouse_motion := ev as InputEventMouseMotion
	if mouse_motion:
		rotate_y(-mouse_motion.relative.x * MOUSE_SENSITIVITY)
		look_dir.y -= mouse_motion.relative.y * MOUSE_SENSITIVITY
		look_dir.y = clamp(look_dir.y, -PI/4, PI/4)

func _physics_process(delta: float):
	if Multiplayer.game_over:
		return
	camera.rotation.x = lerp(camera.rotation.x, look_dir.y, delta * LOOK_SPEED)

	if is_local():
		var move_dir := Vector2(
			Input.get_action_strength("Right") - Input.get_action_strength("Left"),
			Input.get_action_strength("Forward") - Input.get_action_strength("Backward")
		)
		if move_dir.length() > 1.0:
			move_dir = move_dir.normalized()

		motion = motion.move_toward(move_dir, ACCEL * delta)

	anim_tree.set("parameters/move/blend_position", motion)
	anim_tree.set("parameters/speed", 1.8 if is_zombie else 1.5)
	var vel := anim_tree.get_root_motion_transform().origin / delta
	move_and_slide(global_transform.basis.xform(vel))

func get_state() -> Dictionary:
	return {
		"origin": global_transform.origin,
		"motion": motion,
		"hearts": hearts,
		"is_zombie": is_zombie,
	}

func set_state(state: Dictionary):
	if not is_local() or not got_initial_server_state:
		got_initial_server_state = true
		global_transform.origin = state.origin
		motion = state.motion
		hearts = state.hearts
		is_zombie = state.is_zombie
	if not is_local() and state.hearts >= MAX_HEARTS:
		emit_signal("lost")
		Multiplayer.end_game()

func collect_heart():
	if is_local():
		hearts += 1
		if hearts >= MAX_HEARTS:
			emit_signal("won")
			Multiplayer.end_game()
		else:
			emit_signal("heart_collected")

func die():
	emit_signal("died")
	set_collision_layer_bit(1, 0) # human bit off
	yield(get_tree().create_timer(ZOMBIFY_TIME), "timeout")
	set_collision_layer_bit(2, 1) # zombie bit on
	emit_signal("zombified")

func revive():
	emit_signal("revived")
	set_collision_layer_bit(1, 1) # human bit on
	set_collision_layer_bit(2, 0) # zombie bit off
