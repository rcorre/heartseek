extends Camera

func _ready():
	current = owner.is_local()
	set_zfar(20 if owner.is_zombie else 40)
	owner.connect("zombified", self, "set_zfar", [20])
	owner.connect("revived", self, "set_zfar", [40])
