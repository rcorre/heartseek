extends Area

const ACCEL := 3.0

var target: Spatial
var speed: float

func _ready():
	connect("body_entered", self, "_on_body_entered", [], CONNECT_ONESHOT)

func _on_body_entered(body: Spatial):
	# when the player gets close, start moving towards their center
	target = body

func _physics_process(delta: float):
	if target:
		# Move towards the center-ish of the player
		var pos := target.global_transform.origin + Vector3.UP * 2
		speed += delta * ACCEL
		global_transform.origin = global_transform.origin.move_toward(pos, speed * delta)
		if global_transform.origin.distance_to(pos) < 0.1:
			target.collect_heart()
			queue_free()
