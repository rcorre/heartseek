extends Area

func _ready():
	monitoring = owner.is_zombie
	owner.connect("zombified", self, "set_deferred", ["monitoring", true])
	owner.connect("revived", self, "set_deferred", ["monitoring", false])
	connect("body_entered", self, "_on_body_entered")

func _on_body_entered(body: Node):
	if body != owner:
		body.call("die")
		owner.revive()
