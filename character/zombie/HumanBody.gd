extends Spatial

func _ready():
	owner.connect("died", self, "hide")
	owner.connect("revived", self, "show")

	# first player is the spectre
	set_visible(not owner.is_zombie)
